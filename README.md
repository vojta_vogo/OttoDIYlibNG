Ottolib
==========
Library for [OttoDIY style robot](https://www.ottodiy.com), supports Classic and Humanoid variant.

**Project goals:**
- Easy to read source code
- Consistent programming style
- Low memory footprint as much as possible
- Smooth movements without jerking
- Customized 3D model, classic Otto look with full features for both types 

**Built-in support for devices:**
- Buzzer
- Analog noise sensor
- HCSR04 ultrasonic sensor
- TTP223 Touch button
- MAX7219 LED matrix
- MPU6050 Accelerometer/Gyroscope

**Examples:**
- Calibration: sketch for calibrate robot via serial port (need disconnected Bluetooth module)
- App: sketch for control robot over Bluetooth, required additional libraries and software:
  - [SerialCommands](https://github.com/ppedro74/Arduino-SerialCommands) library
  - original [Otto DIY](https://play.google.com/store/apps/details?id=com.ottodiy) application for Android

Resources
===
**Documentation for devices:**
- [HCSR04](https://drive.google.com/open?id=0B4B30jzMyzG8bk80Wk05cmVqdmc)
- [MAX7219](https://www.laskakit.cz/user/related_files/max7219-max7221.pdf)
- [MP6050](https://drive.google.com/open?id=0B4B30jzMyzG8WXBKdVJrZDlnOW8)
- [HC-06](https://drive.google.com/open?id=0B4B30jzMyzG8a1lWaUVvNTd0b2s)

**The code is originally based on or inspired by libraries:**
- [OttoDIYLib](https://github.com/OttoDIY/OttoDIYLib)
- [HCSR04 ultrasonic sensor lib](https://github.com/gamegine/HCSR04-ultrasonic-sensor-lib)
- [MaxMatrix](https://github.com/AndreasBur/MaxMatrix)
- [MPU6050 light](https://github.com/rfetick/MPU6050_light)
