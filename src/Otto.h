#ifndef __OTTO_H__
#define __OTTO_H__

#include <Arduino.h>
#include "OttoConstants.h"
#include "OttoBase.h"
#include "OttoBuzzer.h"
#include "OttoLedMatrix.h"

class Otto : public OttoBase<4>
{
public:
    void init(uint8_t leftLegPin, uint8_t rightLegPin, uint8_t leftFootPin, uint8_t rightFootPin);
    void home(uint32_t period = 0);
    void move(int8_t leftLeg, int8_t rightLeg, int8_t leftFoot, int8_t rightFoot);
    void jump(uint8_t count = 1, uint32_t period = 2000, uint8_t height = 70);
    void bend(OttoDirectionX direction, uint8_t count = 1, uint32_t period = 1000);
    void shakeLeg(OttoDirectionX direction, uint8_t count = 2, uint32_t period = 500);
    void walk(OttoDirectionY direction, uint8_t steps = 4, uint32_t period = 1000);
    void turn(OttoDirectionX direction, uint8_t steps = 4, uint32_t period = 1000);
    void upDown(uint8_t count = 1, uint32_t period = 1000, uint8_t height = 20);
    void swing(uint8_t count = 1, uint32_t period = 500, uint8_t height = 20);
    void tiptoeSwing(uint8_t count = 1, uint32_t period = 500, uint8_t height = 20);
    void jitter(uint8_t count = 1, uint32_t period = 500, uint8_t height = 20);
    void ascendingTurn(uint8_t count = 1, uint32_t period = 900, uint8_t height = 20);
    void moonwalker(OttoDirectionX direction, uint8_t steps = 4, uint32_t period = 900, uint8_t height = 20);
    void crusaito(OttoDirectionX direction, uint8_t steps = 4, uint32_t period = 900, uint8_t height = 20);
    void flapping(OttoDirectionY direction, uint8_t steps = 4, uint32_t period = 1000, uint8_t height = 20);
    void playGesture(OttoGesture gesture);

public:
    OttoBuzzer buzzer;
    OttoLedMatrix ledMatrix;

private:
    const int8_t _homePosition[4] = {0, 0, 0, 0};
};

#endif
