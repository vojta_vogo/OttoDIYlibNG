#include "OttoUltrasonic.h"

#define SPEED_OF_SOUND 0.0343 // cm/us
#define DISTANCE_MAX 400.0
#define DISTANCE_ERR -1.0

void OttoUltrasonic::init(uint8_t triggerPin, uint8_t echoPin)
{
    _triggerPin = triggerPin;
    _echoPin = echoPin;

    pinMode(_triggerPin, OUTPUT);
    pinMode(_echoPin, INPUT);
}

float OttoUltrasonic::measure()
{
    digitalWrite(_triggerPin, LOW);
    delayMicroseconds(2);
    digitalWrite(_triggerPin, HIGH);
    delayMicroseconds(10);
    digitalWrite(_triggerPin, LOW);

    float durationMicroSec = pulseIn(_echoPin, HIGH);
    float distanceCm = (durationMicroSec * SPEED_OF_SOUND) / 2.0;

    if ((distanceCm <= 0.0) || (distanceCm > DISTANCE_MAX))
    {
        distanceCm = DISTANCE_ERR;
    }

    return distanceCm;
}
