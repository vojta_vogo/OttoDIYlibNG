#ifndef __OTTO_HUMANOID_H__
#define __OTTO_HUMANOID_H__

#include <Arduino.h>
#include "OttoConstants.h"
#include "OttoBase.h"
#include "OttoBuzzer.h"
#include "OttoLedMatrix.h"

class OttoHumanoid : public OttoBase<6>
{
public:
    void init(uint8_t leftLegPin, uint8_t rightLegPin, uint8_t leftFootPin, uint8_t rightFootPin, uint8_t leftArmPin, uint8_t rightArmPin);
    void home(uint32_t period = 0);
    void move(int8_t leftLeg, int8_t rightLeg, int8_t leftFoot, int8_t rightFoot, int8_t leftArm, int8_t rightArm);
    void jump(uint8_t count = 1, uint32_t period = 2000, uint8_t height = 70);
    void bend(OttoDirectionX direction, uint8_t count = 1, uint32_t period = 1000);
    void shakeLeg(OttoDirectionX direction, uint8_t count = 2, uint32_t period = 500);
    void walk(OttoDirectionY direction, uint8_t steps = 4, uint32_t period = 1000);
    void turn(OttoDirectionX direction, uint8_t steps = 4, uint32_t period = 1000);
    void upDown(uint8_t count = 1, uint32_t period = 1000, uint8_t height = 20);
    void swing(uint8_t count = 1, uint32_t period = 500, uint8_t height = 20);
    void tiptoeSwing(uint8_t count = 1, uint32_t period = 500, uint8_t height = 20);
    void jitter(uint8_t count = 1, uint32_t period = 500, uint8_t height = 20);
    void ascendingTurn(uint8_t count = 1, uint32_t period = 900, uint8_t height = 20);
    void moonwalker(OttoDirectionX direction, uint8_t steps = 4, uint32_t period = 900, uint8_t height = 20);
    void crusaito(OttoDirectionX direction, uint8_t steps = 4, uint32_t period = 900, uint8_t height = 20);
    void flapping(OttoDirectionY direction, uint8_t steps = 4, uint32_t period = 1000, uint8_t height = 20);
    void moveArms(OttoDirectionZ direction, uint8_t count = 1, uint32_t period = 750, uint8_t height = 70);
    void armWave(OttoDirectionX direction, uint8_t count = 2, uint32_t period = 500);
    void playGesture(OttoGesture gesture);

public:
    OttoBuzzer buzzer;
    OttoLedMatrix ledMatrix;

private:
    const int8_t _homePosition[6] = {0, 0, 0, 0, -75, 75};
};

#endif
