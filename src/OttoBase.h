#ifndef __OTTO_BASE_H__
#define __OTTO_BASE_H__

#include <Arduino.h>
#include <EEPROM.h>
#include "OttoServo.h"
#include "OttoConstants.h"

#define SERVO_INIT_MILLIS 150u
#define SERVO_SAMPLING_MILLIS 2u
#define SERVO_MILLIS_PER_DEG 6u

template <uint8_t N>
class OttoBase
{
public:
    void getName(char name[OTTO_NAME_MAX])
    {
        for (uint8_t i = 0; i < OTTO_NAME_MAX; i++)
        {
            EEPROM.get(EEPROM_OTTO_NAME + (i * sizeof(char)), name[i]);
        }

        name[OTTO_NAME_MAX - 1] = '\0';
    }

    void setName(char name[OTTO_NAME_MAX])
    {
        name[OTTO_NAME_MAX - 1] = '\0';
        
        for (int i = 0; i < OTTO_NAME_MAX; i++)
        {
            EEPROM.put(EEPROM_OTTO_NAME + (i * sizeof(char)), name[i]);
            
            if (name[i] == '\0')
            {
                break;
            }

        }
    }

protected:
    void _initServos(const uint8_t pins[N], const int8_t position0[N])
    {
        for (uint8_t i = 0; i < N; i++)
        {
            int8_t trim = 0;

            EEPROM.get(EEPROM_OSCILLATOR_TRIMS + (i * sizeof(int8_t)), trim);

            _servos[i].init(pins[i]);
            _servos[i].setTrim(trim);
            _servos[i].setPosition(position0[i]);
            _servos[i].attach();
            delay(SERVO_INIT_MILLIS);
            _servos[i].detach();
        }
    }
    
    void _homeServos(const int8_t position[N], uint32_t period)
    {
        _moveServos(position, period);
        _detachServos();
    }

    void _moveServos(const int8_t position[N], uint32_t period)
    {
        int8_t position0[N] = {};

        for (uint8_t i = 0; i < N; i++)
        {
            position0[i] = _servos[i].getPosition();
            period = max(period, SERVO_MILLIS_PER_DEG * abs(position[i] - position0[i]));
        }

        _attachServos();

        for (uint32_t millis0 = millis(); (millis() - millis0) < period; delay(SERVO_SAMPLING_MILLIS))
        {
            for (uint8_t i = 0; i < N; i++)
            {
                _servos[i].setPosition(position0[i] + round((float)(position[i] - position0[i]) * ((float)(millis() - millis0) / (float)period)));
            }
        }

        for (uint8_t i = 0; i < N; i++)
        {
            _servos[i].setPosition(position[i]);
        }

        delay(SERVO_SAMPLING_MILLIS);
    }

    void _oscillateServos(const int8_t offset[N], const int8_t amplitude[N], const int16_t phase0[N], uint8_t count, uint32_t period)
    {
        int8_t position0[N] = {};

        for (uint8_t i = 0; i < N; i++)
        {
            position0[i] = offset[i] + round((float)amplitude[i] * sin(((float)phase0[i] * M_PI) / 180.0));
            period = max(period, SERVO_MILLIS_PER_DEG * abs(4 * amplitude[i]));
        }

        _attachServos();
        _moveServos(position0, 0);

        for (uint32_t millis0 = millis(); (millis() - millis0) < (period * count); delay(SERVO_SAMPLING_MILLIS))
        {
            for (uint8_t i = 0; i < N; i++)
            {
                _servos[i].setPosition(offset[i] + round((float)amplitude[i] * sin((((float)phase0[i] * M_PI) / 180.0) + (2.0 * M_PI * ((float)(millis() - millis0) / (float)period)))));
            }
        }
    }

private:
    void _attachServos()
    {
        for (uint8_t i = 0; i < N; i++)
        {
            _servos[i].attach();
        }
    }

    void _detachServos()
    {
        for (uint8_t i = 0; i < N; i++)
        {
            _servos[i].detach();
        }
    }

private:
    OttoServo _servos[N];
};

#endif
