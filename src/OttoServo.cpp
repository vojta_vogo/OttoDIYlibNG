#include "OttoServo.h"

void OttoServo::init(uint8_t pin)
{
    _pin = pin;
}

void OttoServo::setPosition(int8_t position)
{
    _position = position;

    _servo.write(90 + _trim + _position);
}

void OttoServo::attach()
{
    if (!_servo.attached())
    {
        _servo.attach(_pin);
    }
}

void OttoServo::detach()
{
    if (_servo.attached())
    {
        _servo.detach();
    }
}
