#include "OttoNoise.h"

#define NOISE_READ_COUNT 10
#define NOISE_READ_DELAY 5

void OttoNoise::init(uint8_t pin)
{
    _pin = pin;

    pinMode(_pin, INPUT);
}

uint16_t OttoNoise::measure()
{
    uint16_t value = 0;

    for (uint8_t i = 0; i < NOISE_READ_COUNT; i++)
    {
        value += analogRead(_pin);

        delay(NOISE_READ_DELAY);
    }

    value /= NOISE_READ_COUNT;

    return value;
}
