#ifndef __OTTO_LED_MATRIX_H__
#define __OTTO_LED_MATRIX_H__

#include <Arduino.h>
#include "OttoConstants.h"

class OttoLedMatrix
{
public:
    void init(uint8_t dinPin, uint8_t clkPin, uint8_t csPin);
    void setIntensity(uint8_t intensity);
    void setColumn(uint8_t column, uint8_t data);
    void setDot(uint8_t x, uint8_t y, bool set);
    void shiftLeft(uint8_t data);
    void putBitmap(OttoBitmap bitmap);
    void putBitmap(const uint8_t bitmap[8]);
    void putChar(char c);
    void scrollText(const char *text, uint32_t scrollMillis = 75);
    void clear();

private:
    void _sendCommand(uint8_t command, uint8_t value);

    uint8_t _dinPin = 0;
    uint8_t _clkPin = 0;
    uint8_t _csPin = 0;
    uint8_t _buffer[8] = {};
};

#endif
