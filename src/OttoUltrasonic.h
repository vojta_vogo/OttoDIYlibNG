#ifndef __OTTO_ULTRASONIC_H__
#define __OTTO_ULTRASONIC_H__

#include <Arduino.h>

class OttoUltrasonic
{
public:
    void init(uint8_t triggerPin, uint8_t echoPin);
    float measure();

private:
    uint8_t _triggerPin = 0;
    uint8_t _echoPin = 0;
};

#endif
