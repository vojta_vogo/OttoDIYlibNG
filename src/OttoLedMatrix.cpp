#include "OttoLedMatrix.h"
#include "OttoData.h"

#define MAX7219_REG_NOOP 0x00
#define MAX7219_REG_DIGIT_0 0x01
#define MAX7219_REG_DIGIT_1 0x02
#define MAX7219_REG_DIGIT_2 0x03
#define MAX7219_REG_DIGIT_3 0x04
#define MAX7219_REG_DIGIT_4 0x05
#define MAX7219_REG_DIGIT_5 0x06
#define MAX7219_REG_DIGIT_6 0x07
#define MAX7219_REG_DIGIT_7 0x08
#define MAX7219_REG_DECODE_MODE 0x09
#define MAX7219_REG_INTENSITY 0x0a
#define MAX7219_REG_SCAN_LIMIT 0x0b
#define MAX7219_REG_SHUTDOWN 0x0c
#define MAX7219_REG_DISPLAY_TEST 0x0f

#define DEFAULT_INTENSITY 0x07

void OttoLedMatrix::init(const uint8_t dinPin, uint8_t clkPin, uint8_t csPin)
{
    _dinPin = dinPin;
    _clkPin = clkPin;
    _csPin = csPin;

    pinMode(_dinPin, OUTPUT);
    pinMode(_clkPin, OUTPUT);
    pinMode(_csPin, OUTPUT);
    digitalWrite(_csPin, HIGH);
    _sendCommand(MAX7219_REG_SCAN_LIMIT, 0x07);
    _sendCommand(MAX7219_REG_DECODE_MODE, 0x00);
    _sendCommand(MAX7219_REG_SHUTDOWN, 0x01);
    _sendCommand(MAX7219_REG_DISPLAY_TEST, 0x00);
    setIntensity(DEFAULT_INTENSITY);
}

void OttoLedMatrix::setIntensity(uint8_t intensity)
{
    _sendCommand(MAX7219_REG_INTENSITY, min(0x0f, intensity));
}

void OttoLedMatrix::setColumn(uint8_t column, uint8_t data)
{
    column %= 8;
    _buffer[column] = data;
    _sendCommand(MAX7219_REG_DIGIT_0 + column, data);
}

void OttoLedMatrix::setDot(uint8_t x, uint8_t y, bool set)
{
    x %= 8;
    y %= 8;

    bitWrite(_buffer[x], 7 - y, set);
    _sendCommand(MAX7219_REG_DIGIT_0 + x, _buffer[x]);
}

void OttoLedMatrix::shiftLeft(uint8_t data)
{
    for (uint8_t i = 0; i < 7; i++)
    {
        setColumn(i, _buffer[i + 1]);
    }

    setColumn(7, data);
}

void OttoLedMatrix::putBitmap(OttoBitmap bitmap)
{
    putBitmap(getPROGMEM(&bitmapMap[(uint8_t)bitmap]));
}

void OttoLedMatrix::putBitmap(const uint8_t bitmap[8])
{
    for (uint8_t i = 0; i < 8; i++)
    {
        setColumn(i, bitmap[i]);
    }
}

void OttoLedMatrix::putChar(char c)
{
    clear();

    if ((c < ' ') || (c > '~'))
    {
        c = '?';
    }

    c -= ' ';

    uint16_t startColumn = 0;

    while (c)
    {
        if (bitRead(getPROGMEM(&font[startColumn]), 0))
        {
            c--;
        }

        startColumn++;
    }

    uint16_t endColumn = startColumn;

    while (!bitRead(getPROGMEM(&font[endColumn]), 0))
    {
        endColumn++;
    }

    for (uint8_t i = 0; i <= (endColumn - startColumn); i++)
    {
        setColumn(i + ((8 - (endColumn - startColumn)) / 2), getPROGMEM(&font[startColumn + i]) & 0b11111110);
    }
}

void OttoLedMatrix::scrollText(const char *text, uint32_t scrollMillis /*= 75*/)
{
    clear();

    for (unsigned int i = 0; text[i] != '\0'; i++)
    {
        uint8_t c = text[i];

        if ((c < ' ') || (c > '~'))
        {
            c = '?';
        }

        c -= ' ';

        uint16_t startColumn = 0;

        while (c)
        {
            if (bitRead(getPROGMEM(&font[startColumn]), 0))
            {
                c--;
            }

            startColumn++;
        }

        uint16_t endColumn = startColumn;

        while (!bitRead(getPROGMEM(&font[endColumn]), 0))
        {
            endColumn++;
        }

        for (uint8_t i = 0; i <= (endColumn - startColumn); i++)
        {
            shiftLeft(getPROGMEM(&font[startColumn + i]) & 0b11111110);
            delay(scrollMillis);
        }

        shiftLeft(0b00000000);
        delay(scrollMillis);
    }

    for (uint8_t i = 0; i < 7; i++)
    {
        shiftLeft(0b00000000);
        delay(scrollMillis);
    }
}

void OttoLedMatrix::clear()
{
    for (uint8_t i = 0; i < 8; i++)
    {
        setColumn(i, 0b00000000);
    }
}

void OttoLedMatrix::_sendCommand(uint8_t command, uint8_t value)
{
    digitalWrite(_csPin, LOW);
    shiftOut(_dinPin, _clkPin, MSBFIRST, command);
    shiftOut(_dinPin, _clkPin, MSBFIRST, value);
    digitalWrite(_csPin, HIGH);
}
