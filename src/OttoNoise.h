#ifndef __OTTO_NOISE_H__
#define __OTTO_NOISE_H__

#include <Arduino.h>

class OttoNoise
{
public:
    void init(uint8_t pin);
    uint16_t measure();

private:
    uint8_t _pin = 0;
};

#endif
