#ifndef __OTTO_SERVO_H__
#define __OTTO_SERVO_H__

#include <Arduino.h>
#include <Servo.h>

class OttoServo
{
public:
    void init(uint8_t pin);
    void setTrim(int8_t offset) { _trim = offset; };
    void setPosition(int8_t position);
    int8_t getPosition() { return _position; };
    void attach();
    void detach();

private:
    Servo _servo;
    uint8_t _pin = 0;
    int8_t _trim = 0;
    int8_t _position = 0;
};

#endif
