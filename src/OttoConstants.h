#ifndef __OTTO_CONSTANTS_H__
#define __OTTO_CONSTANTS_H__

#define EEPROM_OSCILLATOR_TRIMS 0
#define EEPROM_ACCEL_GYRO_OFFSETS 6
#define EEPROM_OTTO_NAME 30

#define OTTO_NAME_MAX 16

enum class OttoDirectionX : int8_t
{
    right = -1,
    left = 1
};

template <typename T>
T operator*(OttoDirectionX direction, T i)
{
    return (T)direction * i;
}

enum class OttoDirectionY : int8_t
{
    backward = -1,
    forward = 1
};

template <typename T>
T operator*(OttoDirectionY direction, T i)
{
    return (T)direction * i;
}

enum class OttoDirectionZ : int8_t
{
    down = -1,
    up = 1
};

template <typename T>
T operator*(OttoDirectionZ direction, T i)
{
    return (T)direction * i;
}

enum class OttoOrientation : uint8_t
{
    normal,
    onBack,
    onFront,
    onLeft,
    onRight,
    upsideDown
};

enum class OttoGesture : uint8_t
{
    happy = 1,
    superHappy,
    sad,
    sleeping,
    fart,
    confused,
    love,
    angry,
    fretful,
    magic,
    wave,
    victory,
    fail
};

enum class OttoBitmap : uint8_t
{
    smile,
    happyOpen,
    happyClosed,
    heart,
    bigSurprise,
    smallSurprise,
    tongueOut,
    vamp1,
    vamp2,
    line,
    confused,
    diagonal,
    sad,
    sadOpen,
    sadClosed,
    ok,
    x,
    thunder,
    culito,
    angry,
    up,
    down,
    left,
    right,
    happySmiley,
    sadSmiley,
    neutralSmiley,
    pacman,
    ghost,
    tone,
    bluetooth,
    plug,
    dream0,
    dream1,
    dream2,
    adivinawi0,
    adivinawi1,
    adivinawi2,
    adivinawi3,
    adivinawi4
};

enum class OttoSong : uint8_t
{
    connection = 1,
    disconnection,
    surprise,
    ohOoh,
    ohOoh2,
    cuddly,
    sleeping,
    happy,
    superHappy,
    happyShort,
    sad,
    confused,
    fart1,
    fart2,
    fart3,
    mode1,
    mode2,
    mode3,
    buttonPushed
};

enum class OttoTone : uint8_t
{
    c0,
    db0,
    d0,
    eb0,
    e0,
    f0,
    gb0,
    g0,
    ab0,
    a0,
    bb0,
    b0,
    c1,
    db1,
    d1,
    eb1,
    e1,
    f1,
    gb1,
    g1,
    ab1,
    a1,
    bb1,
    b1,
    c2,
    db2,
    d2,
    eb2,
    e2,
    f2,
    gb2,
    g2,
    ab2,
    a2,
    bb2,
    b2,
    c3,
    db3,
    d3,
    eb3,
    e3,
    f3,
    gb3,
    g3,
    ab3,
    a3,
    bb3,
    b3,
    c4,
    db4,
    d4,
    eb4,
    e4,
    f4,
    gb4,
    g4,
    ab4,
    a4,
    bb4,
    b4,
    c5,
    db5,
    d5,
    eb5,
    e5,
    f5,
    gb5,
    g5,
    ab5,
    a5,
    bb5,
    b5,
    c6,
    db6,
    d6,
    eb6,
    e6,
    f6,
    gb6,
    g6,
    ab6,
    a6,
    bb6,
    b6,
    c7,
    db7,
    d7,
    eb7,
    e7,
    f7,
    gb7,
    g7,
    ab7,
    a7,
    bb7,
    b7,
    c8,
    db8,
    d8,
    eb8
};

#endif
