#include "OttoButton.h"

void OttoButton::init(uint8_t pin)
{
    _pin = pin;

    pinMode(_pin, INPUT);
}

bool OttoButton::isPushed()
{
    return digitalRead(_pin);
}
