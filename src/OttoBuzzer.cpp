#include "OttoBuzzer.h"
#include "OttoData.h"

void OttoBuzzer::init(uint8_t pin)
{
    _pin = pin;
}

void OttoBuzzer::playTone(OttoTone tone, uint32_t toneDuration, uint32_t silentDuration)
{
    playTone(getPROGMEM(&toneMap[(uint8_t)tone]), toneDuration, silentDuration);
}

void OttoBuzzer::playTone(uint16_t frequency, uint32_t toneDuration, uint32_t silentDuration)
{
    if (frequency)
    {
        pinMode(_pin, OUTPUT);

        for (uint32_t millis0 = millis(); (millis() - millis0) < toneDuration;)
        {
            digitalWrite(_pin, HIGH);
            delayMicroseconds(500000 / frequency);
            digitalWrite(_pin, LOW);
            delayMicroseconds(500000 / frequency);
        }

        pinMode(_pin, INPUT);
    }
    else
    {
        delay(toneDuration);
    }

    delay(silentDuration);
}

void OttoBuzzer::bendTones(OttoTone toneBegin, OttoTone toneEnd, float factor, uint32_t toneDuration, uint32_t silentDuration)
{
    bendTones(getPROGMEM(&toneMap[(uint8_t)toneBegin]), getPROGMEM(&toneMap[(uint8_t)toneEnd]), factor, toneDuration, silentDuration);
}

void OttoBuzzer::bendTones(uint16_t frequencyBegin, uint16_t frequencyEnd, float factor, uint32_t toneDuration, uint32_t silentDuration)
{
    if (frequencyBegin < frequencyEnd)
    {
        for (uint16_t i = frequencyBegin; (uint16_t)i < frequencyEnd; i *= factor)
        {
            playTone(i, toneDuration, silentDuration);
        }
    }
    else
    {
        for (uint16_t i = frequencyBegin; (uint16_t)i > frequencyEnd; i /= factor)
        {
            playTone(i, toneDuration, silentDuration);
        }
    }
}

void OttoBuzzer::playSong(OttoSong song)
{
    switch (song)
    {
    case OttoSong::connection:
        playTone(OttoTone::e5, 50, 30);
        playTone(OttoTone::e6, 55, 25);
        playTone(OttoTone::a6, 60, 10);
        break;

    case OttoSong::disconnection:
        playTone(OttoTone::e5, 50, 30);
        playTone(OttoTone::a6, 55, 25);
        playTone(OttoTone::e6, 50, 10);
        break;

    case OttoSong::surprise:
        bendTones(800, 2150, 1.02, 10, 1);
        bendTones(2149, 800, 1.03, 7, 1);
        break;

    case OttoSong::ohOoh:
        bendTones(880, 2000, 1.04, 8, 3);
        delay(200);

        for (uint16_t i = 880; i < 2000; i *= 1.04)
        {
            playTone(OttoTone::b5, 5, 10);
        }
        break;

    case OttoSong::ohOoh2:
        bendTones(1880, 3000, 1.03, 8, 3);
        delay(200);

        for (uint16_t i = 1880; i < 3000; i *= 1.03)
        {
            playTone(OttoTone::c6, 10, 10);
        }
        break;

    case OttoSong::cuddly:
        bendTones(700, 900, 1.03, 16, 4);
        bendTones(899, 650, 1.01, 18, 7);
        break;

    case OttoSong::sleeping:
        bendTones(100, 500, 1.04, 10, 10);
        delay(500);
        bendTones(400, 100, 1.04, 10, 1);
        break;

    case OttoSong::happy:
        bendTones(1500, 2500, 1.05, 20, 8);
        bendTones(2499, 1500, 1.05, 25, 8);
        break;

    case OttoSong::superHappy:
        bendTones(2000, 6000, 1.05, 8, 3);
        delay(50);
        bendTones(5999, 2000, 1.05, 13, 2);
        break;

    case OttoSong::happyShort:
        bendTones(1500, 2000, 1.05, 15, 8);
        delay(100);
        bendTones(1900, 2500, 1.05, 10, 8);
        break;

    case OttoSong::sad:
        bendTones(880, 669, 1.02, 20, 200);
        break;

    case OttoSong::confused:
        bendTones(1000, 1700, 1.03, 8, 2);
        bendTones(1699, 500, 1.04, 8, 3);
        bendTones(1000, 1700, 1.05, 9, 10);
        break;

    case OttoSong::fart1:
        bendTones(1600, 3000, 1.02, 2, 15);
        break;

    case OttoSong::fart2:
        bendTones(2000, 6000, 1.02, 2, 20);
        break;

    case OttoSong::fart3:
        bendTones(1600, 4000, 1.02, 2, 20);
        bendTones(4000, 3000, 1.02, 2, 20);
        break;

    case OttoSong::mode1:
        bendTones(OttoTone::e6, OttoTone::a6, 1.02, 30, 10);
        break;

    case OttoSong::mode2:
        bendTones(OttoTone::g6, OttoTone::d7, 1.03, 30, 10);
        break;

    case OttoSong::mode3:
        playTone(OttoTone::e6, 50, 100);
        playTone(OttoTone::g6, 50, 80);
        playTone(OttoTone::d7, 300, 0);
        break;

    case OttoSong::buttonPushed:
        bendTones(OttoTone::e6, OttoTone::g6, 1.03, 20, 2);
        delay(30);
        bendTones(OttoTone::e6, OttoTone::d7, 1.04, 10, 2);
        break;

    default:
        break;
    }
}
