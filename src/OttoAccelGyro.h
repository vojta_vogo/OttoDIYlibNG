#ifndef __OTTO_ACCEL_GYRO__
#define __OTTO_ACCEL_GYRO__

#include <Arduino.h>
#include "OttoConstants.h"

class OttoAccelGyro
{
public:
    bool init(uint8_t address, bool loadOffsets = true);
    void setOffsets(float accelX, float accelY, float accelZ, float gyroX, float gyroY, float gyroZ);
    void update();
    float getAccelX() { return _accelX; }
    float getAccelY() { return _accelY; }
    float getAccelZ() { return _accelZ; }
    float getGyroX() { return _gyroX; }
    float getGyroY() { return _gyroY; }
    float getGyroZ() { return _gyroZ; }
    float getTemperature() { return _temperature; }
    float getAngleAccelX() { return _angleAccelX; };
    float getAngleAccelY() { return _angleAccelY; };
    float getAngleGyroX() { return _angleGyroX; };
    float getAngleGyroY() { return _angleGyroY; };
    float getAngleGyroZ() { return _angleGyroZ; };
    OttoOrientation getOrientation();

private:
    void _write(uint8_t destination, uint8_t data);

    uint8_t _address = 0x0;
    float _accelX = 0.0;
    float _accelY = 0.0;
    float _accelZ = 0.0;
    float _gyroX = 0.0;
    float _gyroY = 0.0;
    float _gyroZ = 0.0;
    float _angleAccelX = 0.0;
    float _angleAccelY = 0.0;
    float _angleGyroX = 0.0;
    float _angleGyroY = 0.0;
    float _angleGyroZ = 0.0;
    float _temperature = 0.0;
    float _offsetAccelX = 0.0;
    float _offsetAccelY = 0.0;
    float _offsetAccelZ = 0.0;
    float _offsetGyroX = 0.0;
    float _offsetGyroY = 0.0;
    float _offsetGyroZ = 0.0;
    uint32_t _updateMillis = 0;
};

#endif
