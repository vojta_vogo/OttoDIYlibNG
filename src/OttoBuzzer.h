#ifndef __OTTO_BUZZER_H__
#define __OTTO_BUZZER_H__

#include <Arduino.h>
#include "OttoConstants.h"

class OttoBuzzer
{
public:
    void init(uint8_t pin);
    void playTone(OttoTone tone, uint32_t toneDuration, uint32_t silentDuration);
    void playTone(uint16_t frequency, uint32_t toneDuration, uint32_t silentDuration);
    void bendTones(OttoTone toneBegin, OttoTone toneEnd, float factor, uint32_t toneDuration, uint32_t silentDuration);
    void bendTones(uint16_t frequencyBegin, uint16_t frequencyEnd, float factor, uint32_t toneDuration, uint32_t silentDuration);
    void playSong(OttoSong);

private:
    uint8_t _pin = 0;
};

#endif
