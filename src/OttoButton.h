#ifndef __OTTO_BUTTON_H__
#define __OTTO_BUTTON_H__

#include <Arduino.h>

class OttoButton
{
public:
    void init(uint8_t pin);
    bool isPushed();

private:
    uint8_t _pin = 0;
};

#endif
