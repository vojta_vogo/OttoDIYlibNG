#include <Wire.h>
#include <EEPROM.h>
#include "OttoAccelGyro.h"

#define MPU6050_DEVICE_ID 0x68
#define MPU6050_SAMPLE_RATE_DIVISOR_REGISTER 0x19
#define MPU6050_CONFIG_REGISTER 0x1a
#define MPU6050_GYRO_CONFIG_REGISTER 0x1b
#define MPU6050_ACCEL_CONFIG_REGISTER 0x1c
#define MPU6050_ACCEL_OUT_REGISTER 0x3b
#define MPU6050_POWER_MANAGEMET_1_REGISTER 0x6b
#define MPU6050_DEVICE_ID_REGISTER 0x75

#define ACCEL_SCALE 16384.0f
#define GYRO_SCALE 131.0f
#define TEMP_SCALE 340.0f
#define TEMP_OFFSET 36.53f

bool OttoAccelGyro::init(uint8_t address, bool loadOffsets /*= true*/)
{
    _address = address;

    if (loadOffsets)
    {
        for (uint8_t i = 0; i < 6; i++)
        {
            EEPROM.get(EEPROM_ACCEL_GYRO_OFFSETS + (0 * sizeof(float)), _offsetAccelX);
            EEPROM.get(EEPROM_ACCEL_GYRO_OFFSETS + (1 * sizeof(float)), _offsetAccelY);
            EEPROM.get(EEPROM_ACCEL_GYRO_OFFSETS + (2 * sizeof(float)), _offsetAccelZ);
            EEPROM.get(EEPROM_ACCEL_GYRO_OFFSETS + (3 * sizeof(float)), _offsetGyroX);
            EEPROM.get(EEPROM_ACCEL_GYRO_OFFSETS + (4 * sizeof(float)), _offsetGyroY);
            EEPROM.get(EEPROM_ACCEL_GYRO_OFFSETS + (5 * sizeof(float)), _offsetGyroZ);
        }
    }

    Wire.begin();
    Wire.beginTransmission(_address);
    Wire.write(MPU6050_DEVICE_ID_REGISTER);
    Wire.endTransmission();
    Wire.requestFrom(_address, (uint8_t)1);

    if (Wire.read() == MPU6050_DEVICE_ID)
    {
        _write(MPU6050_SAMPLE_RATE_DIVISOR_REGISTER, 0x00);
        _write(MPU6050_CONFIG_REGISTER, 0x00);
        _write(MPU6050_ACCEL_CONFIG_REGISTER, 0x00); // +- 2 g, scale is 16384.0
        _write(MPU6050_GYRO_CONFIG_REGISTER, 0x00);  // +- 250 deg/s, scale is 131.0
        _write(MPU6050_POWER_MANAGEMET_1_REGISTER, 0x01);
        update();

        return true;
    }

    return false;
}

void OttoAccelGyro::setOffsets(float accelX, float accelY, float accelZ, float gyroX, float gyroY, float gyroZ)
{
    _offsetAccelX = accelX;
    _offsetAccelY = accelY;
    _offsetAccelZ = accelZ;
    _offsetGyroX = gyroX;
    _offsetGyroY = gyroY;
    _offsetGyroZ = gyroZ;
}

void OttoAccelGyro::update()
{
    Wire.beginTransmission(_address);
    Wire.write(MPU6050_ACCEL_OUT_REGISTER);
    Wire.endTransmission();

    unsigned long now = millis();
    float period = float(now - _updateMillis) / 1000.0;
    _updateMillis = now;

    if (Wire.requestFrom(_address, (uint8_t)14) == 14)
    {
        _accelX = ((float)((Wire.read() << 8) | Wire.read()) / ACCEL_SCALE) - _offsetAccelX;
        _accelY = ((float)((Wire.read() << 8) | Wire.read()) / ACCEL_SCALE) - _offsetAccelY;
        _accelZ = ((float)((Wire.read() << 8) | Wire.read()) / ACCEL_SCALE) - _offsetAccelZ;
        _temperature = ((float)((Wire.read() << 8) | Wire.read()) / TEMP_SCALE) + TEMP_OFFSET;
        _gyroX = ((float)((Wire.read() << 8) | Wire.read()) / GYRO_SCALE) - _offsetGyroX;
        _gyroY = ((float)((Wire.read() << 8) | Wire.read()) / GYRO_SCALE) - _offsetGyroY;
        _gyroZ = ((float)((Wire.read() << 8) | Wire.read()) / GYRO_SCALE) - _offsetGyroZ;

        float zSign = (_accelZ < 0) ? -1 : 1;
        _angleAccelX = atan2f(_accelY, zSign * sqrt(_accelZ * _accelZ + _accelX * _accelX)) * 180.0 / M_PI;
        _angleAccelY = -atan2f(_accelX, sqrt(_accelZ * _accelZ + _accelY * _accelY)) * 180.0 / M_PI;

        _angleGyroX += _gyroX * period;
        _angleGyroY += zSign * _gyroY * period;
        _angleGyroZ += _gyroZ * period;
    }
}

OttoOrientation OttoAccelGyro::getOrientation()
{
    float angleX = getAngleAccelX();
    float angleY = getAngleAccelY();

    OttoOrientation orientation = OttoOrientation::normal;

    if (angleY > 75.0)
    {
        orientation = OttoOrientation::onLeft;
    }
    else if (angleY < -75.0)
    {
        orientation = OttoOrientation::onRight;
    }
    else if (fabs(angleX) > 135.0)
    {
        orientation = OttoOrientation::upsideDown;
    }
    else if (angleX > 75.0)
    {
        orientation = OttoOrientation::onFront;
    }
    else if (angleX < -75.0)
    {
        orientation = OttoOrientation::onBack;
    }

    return orientation;
}

void OttoAccelGyro::_write(uint8_t destination, uint8_t data)
{
    Wire.beginTransmission(_address);
    Wire.write(destination);
    Wire.write(data);
    Wire.endTransmission();
}
