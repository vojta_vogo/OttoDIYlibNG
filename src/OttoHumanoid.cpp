#include "OttoHumanoid.h"

void OttoHumanoid::init(uint8_t leftLegPin, uint8_t rightLegPin, uint8_t leftFootPin, uint8_t rightFootPin, uint8_t leftArmPin, uint8_t rightArmPin)
{
    const uint8_t pins[6] = {leftLegPin, rightLegPin, leftFootPin, rightFootPin, leftArmPin, rightArmPin};

    _initServos(pins, _homePosition);
}

void OttoHumanoid::home(uint32_t period /*= 0*/)
{
    _homeServos(_homePosition, period);
}

void OttoHumanoid::move(int8_t leftLeg, int8_t rightLeg, int8_t leftFoot, int8_t rightFoot, int8_t leftArm, int8_t rightArm)
{
    const int8_t positions[6] = {leftLeg, rightLeg, leftFoot, rightFoot, leftArm, rightArm};

    _moveServos(positions, 0);
}

void OttoHumanoid::jump(uint8_t count /*= 1*/, uint32_t period /*= 2000*/, uint8_t height /*= 70*/)
{
    const int8_t position1[6] = {0, 0, height, -height, height, -height};

    _moveServos(_homePosition, 0);

    for (uint8_t i = 0; i < count; i++)
    {
        _moveServos(position1, period / 2);
        _moveServos(_homePosition, period / 2);
    }
}

void OttoHumanoid::bend(OttoDirectionX direction, uint8_t count /*= 1*/, uint32_t period /*= 1000*/)
{
    const bool left = direction == OttoDirectionX::left;
    const int8_t position1[6] = {0, 0, left ? -40 : 75, left ? -75 : 40, left ? -50 : 70, left ? -70 : 50};
    const int8_t position2[6] = {0, 0, left ? -40 : -15, left ? 15 : 40, left ? -50 : 70, left ? -70 : 50};

    _moveServos(_homePosition, 0);

    for (int8_t i = 0; i < count; i++)
    {
        _moveServos(position1, 400);
        _moveServos(position2, 400);
        delay(period);
        _moveServos(_homePosition, 0);
    }
}

void OttoHumanoid::shakeLeg(OttoDirectionX direction, uint8_t count /*= 2*/, uint32_t period /*= 500*/)
{
    const bool left = direction == OttoDirectionX::left;
    const int8_t offset[6] = {0, 0, left ? -40 : 30, left ? -30 : 40, left ? -50 : 50, left ? -50 : 50};
    const int8_t amplitude[6] = {0, 0, left ? 0 : 40, left ? 40 : 0, 0, 0};
    const int16_t phase[6] = {0, 0, 90, -90, 0, 0};
    
    _oscillateServos(offset, amplitude, phase, count, period);
}

void OttoHumanoid::walk(OttoDirectionY direction, uint8_t steps /*= 4*/, uint32_t period /*= 1000*/)
{
    const int8_t offset[6] = {0, 0, 4, -4, -55, 55};
    const int8_t amplitude[6] = {30, 30, 20, 20, 20, 20};
    const int16_t phase[6] = {0, 0, direction * -90, direction * -90, 0, 0};

    _oscillateServos(offset, amplitude, phase, steps, period);
}

void OttoHumanoid::turn(OttoDirectionX direction, uint8_t steps /*= 4*/, uint32_t period /*= 1000*/)
{
    const bool left = direction == OttoDirectionX::left;
    const int8_t offset[6] = {0, 0, 4, -4, -55, 55};
    const int8_t amplitude[6] = {left ? 30 : 15, left ? 15 : 30, 20, 20, 20, 20};
    const int16_t phase[6] = {0, 0, -90, -90, 0, 0};

    _oscillateServos(offset, amplitude, phase, steps, period);
}

void OttoHumanoid::upDown(uint8_t count /*= 1*/, uint32_t period /*= 1000*/, uint8_t height /*= 20*/)
{
    const int8_t offset[6] = {0, 0, height, -height, height, -height};
    const int8_t amplitude[6] = {0, 0, height, height, height, height};
    const int16_t phase[6] = {0, 0, -90, 90, -90, 90};

    _oscillateServos(offset, amplitude, phase, count, period);
}

void OttoHumanoid::swing(uint8_t count /*= 1*/, uint32_t period /*= 500*/, uint8_t height /*= 20*/)
{
    const int8_t offset[6] = {0, 0, (height / 2), (-height / 2), height, -height};
    const int8_t amplitude[6] = {0, 0, height, height, height, height};
    const int16_t phase[6] = {0, 0, 0, 0, 0, 0};

    _oscillateServos(offset, amplitude, phase, count, period);
}

void OttoHumanoid::tiptoeSwing(uint8_t count /*= 1*/, uint32_t period /*= 500*/, uint8_t height /*= 20*/)
{
    const int8_t offset[6] = {0, 0, height, -height, height, -height};
    const int8_t amplitude[6] = {height, height, height, height, height, height};
    const int16_t phase[6] = {0, 0, 0, 0, 0, 0};

    _oscillateServos(offset, amplitude, phase, count, period);
}

void OttoHumanoid::jitter(uint8_t count /*= 1*/, uint32_t period /*= 500*/, uint8_t height /*= 20*/)
{
    const int8_t offset[6] = {0, 0, 0, 0, 0, 0};
    const int8_t amplitude[6] = {height, height, 0, 0, 0, 0};
    const int16_t phase[6] = {-90, 90, 0, 0, 0, 0};

    _oscillateServos(offset, amplitude, phase, count, period);
}

void OttoHumanoid::ascendingTurn(uint8_t count /*= 1*/, uint32_t period /*= 900*/, uint8_t height /*= 20*/)
{
    const int8_t offset[6] = {0, 0, height + 4, -height - 4, 0, 0};
    const int8_t amplitude[6] = {height, height, height, height, 40, 40};
    const int16_t phase[6] = {-90, 90, -90, -90, 0, 0};

    _oscillateServos(offset, amplitude, phase, count, period);
}

void OttoHumanoid::moonwalker(OttoDirectionX direction, uint8_t steps /*= 4*/, uint32_t period /*= 900*/, uint8_t height /*= 20*/)
{
    const int8_t offset[6] = {0, 0, (height / 2) + 2, -(height / 2) - 2, height, -height};
    const int8_t amplitude[6] = {0, 0, height, height, height, height};
    const int16_t phase[6] = {0, 0, direction * -90, direction * -150, direction * -90, direction * -90};

    _oscillateServos(offset, amplitude, phase, steps, period);
}

void OttoHumanoid::crusaito(OttoDirectionX direction, uint8_t steps /*= 4*/, uint32_t period /*= 900*/, uint8_t height /*= 20*/)
{
    const bool left = direction == OttoDirectionX::left;
    const int8_t offset[6] = {0, 0, (height / 2) + 4, -(height / 2) - 4, 0, 0};
    const int8_t amplitude[6] = {25, 25, height, height, height, height};
    const int16_t phase[6] = {90, 90, left ? 0 : 60, left ? 60 : 0, 0, 0};

    _oscillateServos(offset, amplitude, phase, steps, period);
}

void OttoHumanoid::flapping(OttoDirectionY direction, uint8_t steps /*= 4*/, uint32_t period /*= 1000*/, uint8_t height /*= 20*/)
{
    const int8_t offset[6] = {0, 0, height + 10, -height - 10, 0, 0};
    const int8_t amplitude[6] = {12, 12, height, height, height, -height};
    const int16_t phase[6] = {0, 180, direction * -90, direction * 90, 0, 0};

    _oscillateServos(offset, amplitude, phase, steps, period);
}

void OttoHumanoid::moveArms(OttoDirectionZ direction, uint8_t count /*= 1*/, uint32_t period /*= 750*/, uint8_t height /*= 70*/)
{
    const int8_t position1[6] = {0, 0, 0, 0, direction * height, direction * -height};

    _moveServos(_homePosition, 0);

    for (uint8_t i = 0; i < count; i++)
    {
        _moveServos(position1, period / 2);
        _moveServos(_homePosition, period / 2);
    }
}

void OttoHumanoid::armWave(OttoDirectionX direction, uint8_t count /*= 2*/, uint32_t period /*= 500*/)
{
    const bool left = direction == OttoDirectionX::left;
    const int8_t offset[6] = {0, 0, 0, 0, direction * 50, direction * 50};
    const int8_t amplitude[6] = {0, 0, 0, 0, (left ? 30 : 0), (left ? 0 : 30)};
    const int16_t phase[6] = {0, 0, 0, 0, 0, 0};

    _oscillateServos(offset, amplitude, phase, count, period);
}

void OttoHumanoid::playGesture(OttoGesture gesture)
{
    int8_t position1[6] = {0, 0, 0, 0, 0, 0};
    
    ledMatrix.clear();

    switch (gesture)
    {
    case OttoGesture::happy:
        buzzer.playTone(OttoTone::e5, 50, 30);
        ledMatrix.putBitmap(OttoBitmap::smile);
        buzzer.playSong(OttoSong::happyShort);
        swing(1, 800, 20);
        buzzer.playSong(OttoSong::happyShort);
        break;

    case OttoGesture::superHappy:
        ledMatrix.putBitmap(OttoBitmap::happyOpen);
        buzzer.playSong(OttoSong::happy);
        ledMatrix.putBitmap(OttoBitmap::happyClosed);
        tiptoeSwing(1, 500, 20);
        ledMatrix.putBitmap(OttoBitmap::happyOpen);
        buzzer.playSong(OttoSong::superHappy);
        ledMatrix.putBitmap(OttoBitmap::happyClosed);
        tiptoeSwing(1, 500, 20);
        break;

    case OttoGesture::sad:
        position1[0] = 30;
        position1[1] = -30;
        position1[2] = -70;
        position1[3] = 70;
        position1[4] = -30;
        position1[5] = 30;

        ledMatrix.putBitmap(OttoBitmap::sad);
        _moveServos(position1, 700);
        buzzer.bendTones(880, 830, 1.02, 20, 200);
        ledMatrix.putBitmap(OttoBitmap::sadClosed);
        buzzer.bendTones(830, 790, 1.02, 20, 200);
        ledMatrix.putBitmap(OttoBitmap::sadOpen);
        buzzer.bendTones(790, 740, 1.02, 20, 200);
        ledMatrix.putBitmap(OttoBitmap::sadClosed);
        buzzer.bendTones(740, 700, 1.02, 20, 200);
        ledMatrix.putBitmap(OttoBitmap::sadOpen);
        buzzer.bendTones(700, 669, 1.02, 20, 200);
        ledMatrix.putBitmap(OttoBitmap::sad);
        delay(500);
        _moveServos(_homePosition, 0);
        delay(300);
        break;

    case OttoGesture::sleeping:
        position1[0] = 10;
        position1[1] = -10;
        position1[2] = -30;
        position1[3] = 30;
        position1[4] = -20;
        position1[5] = 20;

        _moveServos(position1, 700);

        for (uint8_t i = 0; i < 4; i++)
        {
            ledMatrix.putBitmap(OttoBitmap::dream0);
            buzzer.bendTones(100, 200, 1.04, 10, 10);
            ledMatrix.putBitmap(OttoBitmap::dream1);
            buzzer.bendTones(200, 300, 1.04, 10, 10);
            ledMatrix.putBitmap(OttoBitmap::dream2);
            buzzer.bendTones(300, 500, 1.04, 10, 10);
            delay(500);
            ledMatrix.putBitmap(OttoBitmap::dream1);
            buzzer.bendTones(400, 250, 1.04, 10, 1);
            ledMatrix.putBitmap(OttoBitmap::dream0);
            buzzer.bendTones(250, 100, 1.04, 10, 1);
            delay(500);
        }

        ledMatrix.putBitmap(OttoBitmap::line);
        buzzer.playSong(OttoSong::cuddly);
        break;

    case OttoGesture::fart:
        position1[2] = 55;
        position1[3] = 32;
        position1[5] = 30;

        _moveServos(position1, 500);
        delay(300);
        ledMatrix.putBitmap(OttoBitmap::line);
        buzzer.playSong(OttoSong::fart1);
        ledMatrix.putBitmap(OttoBitmap::tongueOut);
        delay(250);

        position1[2] = -10;
        position1[4] = 20;

        _moveServos(position1, 500);
        delay(300);
        ledMatrix.putBitmap(OttoBitmap::line);
        buzzer.playSong(OttoSong::fart2);
        ledMatrix.putBitmap(OttoBitmap::tongueOut);
        delay(250);

        position1[2] = 55;
        position1[3] = -10;
        position1[4] = -55;
        position1[5] = 0;

        _moveServos(position1, 500);
        delay(300);
        ledMatrix.putBitmap(OttoBitmap::line);
        buzzer.playSong(OttoSong::fart3);
        ledMatrix.putBitmap(OttoBitmap::tongueOut);
        delay(300);
        _moveServos(_homePosition, 0);
        delay(500);
        break;

    case OttoGesture::confused:
        position1[0] = 20;
        position1[1] = -20;
        position1[4] = -20;
        position1[5] = 20;

        _moveServos(position1, 300);
        ledMatrix.putBitmap(OttoBitmap::confused);
        buzzer.playSong(OttoSong::confused);
        delay(500);
        break;

    case OttoGesture::love:
        ledMatrix.putBitmap(OttoBitmap::heart);
        buzzer.playSong(OttoSong::cuddly);
        crusaito(OttoDirectionX::left, 2, 900, 15);
        _moveServos(_homePosition, 0);
        buzzer.playSong(OttoSong::happyShort);
        break;

    case OttoGesture::angry:
        position1[2] = -20;
        position1[3] = 20;
        position1[4] = -20;
        position1[5] = 20;

        _moveServos(position1, 300);
        ledMatrix.putBitmap(OttoBitmap::angry);
        buzzer.playTone(OttoTone::a5, 100, 30);
        buzzer.bendTones(OttoTone::a5, OttoTone::d6, 1.02, 7, 4);
        buzzer.bendTones(OttoTone::d6, OttoTone::g6, 1.02, 10, 1);
        buzzer.bendTones(OttoTone::g6, OttoTone::a5, 1.02, 10, 1);
        delay(15);
        buzzer.bendTones(OttoTone::a5, OttoTone::e5, 1.02, 20, 4);
        delay(400);

        position1[0] = 20;
        position1[1] = 20;
        position1[2] = 0;
        position1[3] = 0;
        position1[4] = -60;
        position1[5] = 60;

        _moveServos(position1, 200);
        buzzer.bendTones(OttoTone::a5, OttoTone::d6, 1.02, 20, 4);

        position1[0] = -20;
        position1[1] = -20;

        _moveServos(position1, 200);
        buzzer.bendTones(OttoTone::a5, OttoTone::e5, 1.02, 20, 4);
        break;

    case OttoGesture::fretful:
        position1[4] = -60;
        position1[5] = 60;

        ledMatrix.putBitmap(OttoBitmap::angry);
        _moveServos(position1, 0);
        buzzer.bendTones(OttoTone::a5, OttoTone::d6, 1.02, 20, 4);
        buzzer.bendTones(OttoTone::a5, OttoTone::e5, 1.02, 20, 4);
        delay(300);
        ledMatrix.putBitmap(OttoBitmap::line);

        for (int i = 0; i < 4; i++)
        {
            position1[3] = 20;
            _moveServos(position1, 100);
            position1[3] = 0;
            _moveServos(position1, 0);
        }

        ledMatrix.putBitmap(OttoBitmap::angry);
        delay(500);
        break;

    case OttoGesture::magic:
        for (int i = 0; i < 4; i++)
        {
            int16_t baseTone = 400;

            for (uint8_t j = 0; j < 6; j++)
            {
                if (j < 5)
                {
                    ledMatrix.putBitmap((OttoBitmap)((uint8_t)OttoBitmap::adivinawi0 + j));
                }
                else
                {
                    ledMatrix.clear();
                }

                for (uint8_t k = 0; k < 5; k++)
                {
                    buzzer.bendTones(baseTone, baseTone + 25, 1.04, 10, 0);

                    position1[4]++;
                    position1[5]--;

                    _moveServos(position1, 0);

                    baseTone += 25;
                }
            }

            ledMatrix.clear();
            buzzer.bendTones(baseTone - 100, baseTone + 100, 1.04, 10, 10);

            for (uint8_t j = 0; j < 6; j++)
            {
                if (j < 5)
                {
                    ledMatrix.putBitmap((OttoBitmap)((uint8_t)OttoBitmap::adivinawi0 + j));
                }
                else
                {
                    ledMatrix.clear();
                }

                for (uint8_t k = 0; k < 5; k++)
                {
                    buzzer.bendTones(baseTone, baseTone + 25, 1.04, 10, 0);

                    position1[4]--;
                    position1[5]++;

                    _moveServos(position1, 0);

                    baseTone -= 25;
                }
            }
        }

        delay(300);
        break;

    case OttoGesture::wave:
        position1[4] = -18;
        position1[5] = 18;

        ledMatrix.clear();
        _moveServos(position1, 0);

        for (uint8_t i = 0, a = 0; i < 2; i++)
        {
            int baseTone = 500;

            for (uint8_t j = 0; j < 24; j++, a = (a + 1) % 8)
            {
                ledMatrix.shiftLeft(0b1 << (3 + ((a < 4) ? a : 7 - a)));

                for (uint8_t k = 0; k < 2; k++)
                {
                    buzzer.bendTones(baseTone, baseTone + 50, 1.02, 10, 0);

                    position1[4]++;
                    position1[5]--;

                    _moveServos(position1, 0);

                    baseTone += 50;
                }
            }

            for (uint8_t j = 0; j < 24; j++, a = (a + 1) % 8)
            {
                ledMatrix.shiftLeft(0b1 << (3 + ((a < 4) ? a : 7 - a)));

                for (uint8_t k = 0; k < 2; k++)
                {
                    buzzer.bendTones(baseTone, baseTone - 50, 1.02, 10, 0);

                    position1[4]--;
                    position1[5]++;

                    _moveServos(position1, 0);

                    baseTone -= 50;
                }
            }
        }

        for (uint8_t i = 0; i < 8; i++)
        {
            ledMatrix.shiftLeft(0b00000000);
            delay(115);
        }

        delay(100);
        break;

    case OttoGesture::victory:
        position1[4] = -60;
        position1[5] = 60;

        ledMatrix.putBitmap(OttoBitmap::smallSurprise);
        _moveServos(position1, 0);

        for (uint8_t i = 0; i < 60; i++)
        {
            position1[4]++;
            position1[5]--;

            _moveServos(position1, 10);
            buzzer.playTone(1600 + (i * 20), 15, 1);
        }

        ledMatrix.putBitmap(OttoBitmap::bigSurprise);

        for (uint8_t i = 0; i < 60; i++)
        {
            position1[4]++;
            position1[5]--;

            _moveServos(position1, 10);
            buzzer.playTone(2800 + (i * 20), 15, 1);
        }

        ledMatrix.putBitmap(OttoBitmap::happyOpen);
        tiptoeSwing(1, 500, 20);
        buzzer.playSong(OttoSong::superHappy);
        ledMatrix.putBitmap(OttoBitmap::happyClosed);
        tiptoeSwing(1, 500, 20);
        break;

    case OttoGesture::fail:
        ledMatrix.putBitmap(OttoBitmap::sad);

        position1[2] = -10;
        position1[3] = -40;
        position1[4] = -10;
        position1[5] = 10;

        _moveServos(position1, 300);
        buzzer.playTone(900, 200, 1);
        ledMatrix.putBitmap(OttoBitmap::sadClosed);

        position1[2] = -20;
        position1[3] = -50;
        position1[4] = -25;
        position1[5] = 25;

        _moveServos(position1, 300);
        buzzer.playTone(600, 200, 1);
        ledMatrix.putBitmap(OttoBitmap::confused);

        position1[2] = -30;
        position1[3] = -60;
        position1[4] = -40;
        position1[5] = 40;

        _moveServos(position1, 300);
        buzzer.playTone(300, 200, 1);

        position1[2] = -40;
        position1[3] = -70;
        position1[4] = -55;
        position1[5] = 55;

        _moveServos(position1, 300);
        ledMatrix.putBitmap(OttoBitmap::x);
        buzzer.playTone(150, 2200, 1);
        delay(600);
        _moveServos(_homePosition, 500);
        break;

    default:
        break;
    }

    home();
}
