#include <EEPROM.h>
#include <OttoConfig.h>
#include <OttoBase.h>
#include <OttoServo.h>
#include <OttoAccelGyro.h>
#include <OttoConstants.h>

#define ACCEL_GYRO_CALIBRATION_READINGS_COUNT 500
#define ACCEL_GYRO_CALIBRATION_READINGS_DELAY 2

void mainMessage()
{
    Serial.println("'s' - calibrate servos");
    Serial.println("'g' - calibrate accel-gyro");
    Serial.println("'n' - get/set name");
    Serial.println("'h' - this message");
    Serial.println("'Q' - quit");
}

void servoMessage()
{
    Serial.println("'E' - read trims from EEPROM");
    Serial.println("'W' - write trims to EEPROM");
    Serial.println("'L' - select left leg");
    Serial.println("'l' - select right leg");
    Serial.println("'F' - select left foot");
    Serial.println("'f' - select right foot");
    Serial.println("'A' - select left amd");
    Serial.println("'a' - select right arm");
    Serial.println("'+' - add to the trim");
    Serial.println("'-' - subtract from the trim");
    Serial.println("'h' - this message");
    Serial.println("'m' - back to main menu");
}

void accelGyroMessage()
{
    Serial.println("'E' - read offsets from EEPROM");
    Serial.println("'W' - write offsets to EEPROM");
    Serial.println("'c' - calibratate");
    Serial.println("'h' - this message");
    Serial.println("'m' - back to main menu");
}

void nameMessage()
{
    Serial.println("'E' - read name from EEPROM");
    Serial.println("'W' - write name to EEPROM");
    Serial.println("'g' - get name");
    Serial.println("'s' - set name");
    Serial.println("'h' - this message");
    Serial.println("'m' - back to main menu");
}

void calibrateServos()
{
    Serial.println("Calibrate servos...");
    servoMessage();

    const uint8_t pins[6] = {OTTO_LEFT_LEG_PIN, OTTO_RIGHT_LEG_PIN, OTTO_LEFT_FOOT_PIN, OTTO_RIGHT_FOOT_PIN, OTTO_LEFT_ARM_PIN, OTTO_RIGHT_ARM_PIN};
    uint8_t servo = 0;
    int8_t trims[6] = {0, 0, 0, 0, 0, 0};
    OttoServo servos[6];

    for (uint8_t i = 0; i < 6; i++)
    {
        servos[i].init(pins[i]);
        servos[i].attach();
    }

    for (;;)
    {
        if (Serial.available())
        {
            switch (Serial.read())
            {
            case 'E':
                for (uint8_t i = 0; i < 6; i++)
                {
                    EEPROM.get(EEPROM_OSCILLATOR_TRIMS + (i * sizeof(int8_t)), trims[i]);
                    servos[i].setTrim(trims[i]);
                    servos[i].setPosition(0);
                }

                Serial.println("Trims loaded from EEPROM!");
                break;

            case 'W':
                for (int i = 0; i < 6; i++)
                {
                    EEPROM.put(EEPROM_OSCILLATOR_TRIMS + (i * sizeof(int8_t)), trims[i]);
                }

                Serial.println("Trims written to EEPROM!");
                break;

            case 'L':
                Serial.println("Selected left leg");
                servo = 0;
                break;

            case 'l':
                Serial.println("Selected right leg");
                servo = 1;
                break;
            case 'F':
                Serial.println("Selected left foot");
                servo = 2;
                break;

            case 'f':
                Serial.println("Selected right foot");
                servo = 3;
                break;

            case 'A':
                Serial.println("Selected left arm");
                servo = 4;
                break;

            case 'a':
                Serial.println("Selected right arm");
                servo = 5;
                break;

            case '+':
                if (trims[servo] < INT8_MAX)
                {
                    trims[servo]++;
                }

                for (uint8_t i = 0; i < 6; i++)
                {
                    servos[i].setTrim(trims[i]);
                    servos[i].setPosition(0);
                }
                break;

            case '-':
                if (trims[servo] > INT8_MIN)
                {
                    trims[servo]--;
                }

                for (uint8_t i = 0; i < 6; i++)
                {
                    servos[i].setTrim(trims[i]);
                    servos[i].setPosition(0);
                }
                break;

            case 'm':
                for (uint8_t i = 0; i < 6; i++)
                {
                    servos[i].detach();
                }

                return;
                break;

            case 'h':
                servoMessage();
                break;

            default:
                break;
            }
        }
    }
}

void calibrateAccelGyro()
{
    Serial.println("Calibrate accel-gyro...");
    accelGyroMessage();

    float offsets[6] = {0, 0, 0, 0, 0, 0};
    OttoAccelGyro accelGyro;

    if (!accelGyro.init(OTTO_ACCEL_GYRO_ADDRESS, false))
    {
        Serial.println("Initializing FAILED!");
        return;
    }

    for (;;)
    {
        if (Serial.available())
        {
            switch (Serial.read())
            {
            case 'E':
                for (uint8_t i = 0; i < 6; i++)
                {
                    EEPROM.get(EEPROM_ACCEL_GYRO_OFFSETS + (i * sizeof(float)), offsets[i]);
                }

                accelGyro.setOffsets(offsets[0], offsets[1], offsets[2], offsets[3], offsets[4], offsets[5]);
                Serial.println("Offsets loaded from EEPROM!");
                break;

            case 'W':
                for (int i = 0; i < 6; i++)
                {
                    EEPROM.put(EEPROM_ACCEL_GYRO_OFFSETS + (i * sizeof(float)), offsets[i]);
                }

                Serial.println("Offsets written to EEPROM!");
                break;

            case 'c':
                Serial.println("Place robot horizontally and do not move it!");

                for (uint8_t i = 40; i > 0; i--)
                {
                    if (!(i % 4))
                    {
                        Serial.print(i / 4);
                    }
                    else
                    {
                        Serial.print(".");
                    }

                    delay(250);
                }

                Serial.println();
                Serial.println("Measuring offsets...");

                for (int i = 0; i < 6; i++)
                {
                    offsets[i] = 0.0;
                }

                accelGyro.setOffsets(offsets[0], offsets[1], offsets[2], offsets[3], offsets[4], offsets[5]);
                accelGyro.update();

                for (uint16_t i = 0; i < ACCEL_GYRO_CALIBRATION_READINGS_COUNT; i++)
                {
                    delay(ACCEL_GYRO_CALIBRATION_READINGS_DELAY);
                    accelGyro.update();

                    offsets[0] += accelGyro.getAccelX();
                    offsets[1] += accelGyro.getAccelY();
                    offsets[2] += accelGyro.getAccelZ() - 1.0;
                    offsets[3] += accelGyro.getGyroX();
                    offsets[4] += accelGyro.getGyroY();
                    offsets[5] += accelGyro.getGyroZ();
                }

                for (int i = 0; i < 6; i++)
                {
                    offsets[i] /= (float)ACCEL_GYRO_CALIBRATION_READINGS_COUNT;
                }

                accelGyro.setOffsets(offsets[0], offsets[1], offsets[2], offsets[3], offsets[4], offsets[5]);
                Serial.println("Done!");
                break;

            case 'm':
                return;
                break;

            case 'h':
                accelGyroMessage();
                break;

            default:
                break;
            }
        }
    }
}

void getSetName()
{
    Serial.println("Get/set name...");
    nameMessage();

    OttoBase<0> otto;

    char name[OTTO_NAME_MAX];

    for (;;)
    {
        if (Serial.available())
        {
            switch (Serial.read())
            {
            case 'E':
                otto.getName(name);
                Serial.println("Name loaded from EEPROM!");
                break;

            case 'W':                
                otto.setName(name);
                Serial.println("Name written to EEPROM!");
                break;

            case 'g':
                Serial.print("Name is: ");
                Serial.println(name);
                break;

            case 's':
                Serial.println("Write new name");
                Serial.setTimeout(5000);
                Serial.readString().toCharArray(name, OTTO_NAME_MAX);
                Serial.print("New name is: ");
                Serial.println(name);
                break;

            case 'm':
                return;
                break;

            case 'h':
                accelGyroMessage();
                break;

            default:
                break;
            }
        }
    }
}

void setup()
{
    Serial.begin(115200);
    Serial.println("Welcome in calibration wizard!");
    Serial.println("The interface uses single character controls");
    mainMessage();

    for (;;)
    {
        if (Serial.available())
        {
            switch (Serial.read())
            {
            case 's':
                calibrateServos();
                mainMessage();
                break;

            case 'g':
                calibrateAccelGyro();
                mainMessage();
                break;

            case 'n':
                getSetName();
                mainMessage();
                break;

            case 'h':
                mainMessage();
                break;

            case 'Q':
                return;
                break;

            default:
                break;
            }
        }
    }
}

void loop()
{
}
