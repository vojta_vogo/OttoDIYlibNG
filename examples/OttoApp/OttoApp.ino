#include <OttoConfig.h>
#include <Otto.h>
#include <OttoUltrasonic.h>
#include <OttoNoise.h>
#include <SerialCommands.h>

#define PROGRAM_ID "Otto_APP_V11"

Otto otto;
OttoUltrasonic ultrasonic;
OttoNoise noise;

int moveId = -1;
int movePeriod = 0;
int moveSize = 0;

void move()
{
    switch (moveId)
    {
    default:
    case 0:
        otto.home();

        moveId = -1;
        break;

    case 1:
        otto.walk(OttoDirectionY::forward, 1, movePeriod);
        break;

    case 2:
        otto.walk(OttoDirectionY::backward, 1, movePeriod);
        break;

    case 3:
        otto.turn(OttoDirectionX::left, 1, movePeriod);
        break;

    case 4:
        otto.turn(OttoDirectionX::right, 1, movePeriod);
        break;

    case 5:
        otto.upDown(1, movePeriod, moveSize);
        break;

    case 6:
        otto.moonwalker(OttoDirectionX::right, 1, movePeriod, moveSize);
        break;

    case 7:
        otto.moonwalker(OttoDirectionX::left, 1, movePeriod, moveSize);
        break;

    case 8:
        otto.swing(1, movePeriod, moveSize);
        break;

    case 9:
        otto.crusaito(OttoDirectionX::left, 1, movePeriod, moveSize);
        break;

    case 10:
        otto.crusaito(OttoDirectionX::right, 1, movePeriod, moveSize);
        break;

    case 11:
        otto.jump(1, movePeriod);
        break;

    case 12:
        otto.flapping(OttoDirectionY::forward, 1, movePeriod, moveSize);
        break;

    case 13:
        otto.flapping(OttoDirectionY::backward, 1, movePeriod, moveSize);
        break;

    case 14:
        otto.tiptoeSwing(1, movePeriod, moveSize);
        break;

    case 15:
        otto.bend(OttoDirectionX::right, 1, movePeriod);
        break;

    case 16:
        otto.bend(OttoDirectionX::left, 1, movePeriod);
        break;

    case 17:
        otto.shakeLeg(OttoDirectionX::right, 2, movePeriod);
        break;

    case 18:
        otto.shakeLeg(OttoDirectionX::left, 2, movePeriod);
        break;

    case 19:
        otto.jitter(1, movePeriod, moveSize);
        break;

    case 20:
        otto.ascendingTurn(1, movePeriod, moveSize);
        break;
    }
}

void defaultHandler(SerialCommands *sender, const char *command)
{
    receiveStop(sender);
}

void receiveStop(SerialCommands *sender)
{
    moveId = -1;

    sendAck();
    otto.home();
    sendFinalAck();
}

void receiveLED(SerialCommands *sender)
{
    moveId = -1;

    sendAck();
    otto.home();

    char *argument0 = sender->Next();

    if (argument0)
    {
        uint32_t ledMask = strtoul(argument0, NULL, 2);

        otto.ledMatrix.clear();

        for (int row = 0; row < 5; row++)
        {
            for (int column = 0; column < 6; column++)
            {
                otto.ledMatrix.setDot(6 - column, row, (0x1 & (ledMask >> ((row * 6) + column))));
            }
        }
    }
    else
    {
        otto.ledMatrix.putBitmap(OttoBitmap::x);
        delay(2000);
        otto.ledMatrix.putBitmap(OttoBitmap::smile);
    }

    sendFinalAck();
}

void receiveBuzzer(SerialCommands *sender)
{
    moveId = -1;

    sendAck();
    otto.home();

    char *argument0 = sender->Next();
    char *argument1 = sender->Next();

    if (argument0 && argument1)
    {
        otto.buzzer.playTone(atoi(argument0), atol(argument1), 0);
    }
    else
    {
        otto.ledMatrix.putBitmap(OttoBitmap::x);
        delay(2000);
        otto.ledMatrix.putBitmap(OttoBitmap::smile);
    }

    sendFinalAck();
}

void receiveMovement(SerialCommands *sender)
{
    sendAck();

    moveId = -1;

    char *argument0 = sender->Next();
    char *argument1 = sender->Next();
    char *argument2 = sender->Next();

    if (argument0)
    {
        moveId = atoi(argument0);

        if (argument1)
        {
            movePeriod = atoi(argument1);
        }
        else
        {
            movePeriod = 1000;
        }

        if (argument2)
        {
            moveSize = atoi(argument2);
        }
        else
        {
            moveSize = 20;
        }

        move();
    }
    else
    {
        otto.ledMatrix.putBitmap(OttoBitmap::x);
        delay(2000);
        otto.ledMatrix.putBitmap(OttoBitmap::smile);
    }

    sendFinalAck();
}

void receiveGesture(SerialCommands *sender)
{
    moveId = -1;

    sendAck();
    otto.home();

    char *argument0 = sender->Next();

    if (argument0)
    {
        otto.playGesture((OttoGesture)atoi(argument0));
        otto.ledMatrix.putBitmap(OttoBitmap::smile);
    }
    else
    {
        otto.ledMatrix.putBitmap(OttoBitmap::x);
        delay(2000);
        otto.ledMatrix.putBitmap(OttoBitmap::smile);
    }

    sendFinalAck();
}

void receiveSong(SerialCommands *sender)
{
    moveId = -1;

    sendAck();
    otto.home();

    char *argument0 = sender->Next();

    if (argument0)
    {
        otto.buzzer.playSong((OttoSong)atoi(argument0));
    }
    else
    {
        otto.ledMatrix.putBitmap(OttoBitmap::x);
        delay(2000);
        otto.ledMatrix.putBitmap(OttoBitmap::smile);
    }

    sendFinalAck();
}

void receiveServo(SerialCommands *sender)
{
    moveId = -1;

    sendAck();
    otto.home();

    char *argument0 = sender->Next();
    char *argument1 = sender->Next();
    char *argument2 = sender->Next();
    char *argument3 = sender->Next();

    if (argument0 && argument1 && argument2 && argument3)
    {
        otto.move(atoi(argument0), atoi(argument1), atoi(argument2), atoi(argument3));
    }
    else
    {
        otto.ledMatrix.putBitmap(OttoBitmap::x);
        delay(2000);
        otto.ledMatrix.putBitmap(OttoBitmap::smile);
    }

    sendFinalAck();
}

void receiveName(SerialCommands *sender)
{
    char name[16] = {};

    moveId = -1;

    sendAck();
    otto.home();

    char *argument0 = sender->Next();

    if (argument0)
    {
        strncpy(name, argument0, OTTO_NAME_MAX);
        otto.setName(name);
    }
    else
    {
        otto.ledMatrix.putBitmap(OttoBitmap::x);
        delay(2000);
        otto.ledMatrix.clear();
    }

    sendFinalAck();
}

void requestName(SerialCommands *sender)
{
    char name[16] = {};

    moveId = -1;

    otto.home();
    otto.getName(name);
    Serial.print("&&");
    Serial.print("E ");
    Serial.print(name);
    Serial.println("%%");
    Serial.flush();
}

void requestDistance(SerialCommands *sender)
{
    moveId = -1;

    otto.home();
    Serial.print("&&");
    Serial.print("D ");
    Serial.print((int)ultrasonic.measure());
    Serial.println("%%");
    Serial.flush();
}

void requestNoise(SerialCommands *sender)
{
    moveId = -1;

    otto.home();
    Serial.print("&&");
    Serial.print("N ");
    Serial.print(noise.measure());
    Serial.println("%%");
    Serial.flush();
}

void requestProgramId(SerialCommands *sender)
{
    moveId = -1;

    otto.home();
    Serial.print("&&");
    Serial.print("I ");
    Serial.print(PROGRAM_ID);
    Serial.println("%%");
    Serial.flush();
}

void sendAck()
{
    Serial.print("&&");
    Serial.print("A");
    Serial.println("%%");
    Serial.flush();
}

void sendFinalAck()
{
    Serial.print("&&");
    Serial.print("F");
    Serial.println("%%");
    Serial.flush();
}

char buffer[255];

SerialCommands serialCommands(&Serial, buffer, sizeof(buffer), "\r", " ");
SerialCommand serialCommandS("S", receiveStop);
SerialCommand serialCommandL("L", receiveLED);
SerialCommand serialCommandT("T", receiveBuzzer);
SerialCommand serialCommandM("M", receiveMovement);
SerialCommand serialCommandH("H", receiveGesture);
SerialCommand serialCommandK("K", receiveSong);
SerialCommand serialCommandG("G", receiveServo);
SerialCommand serialCommandR("R", receiveName);
SerialCommand serialCommandE("E", requestName);
SerialCommand serialCommandD("D", requestDistance);
SerialCommand serialCommandN("N", requestNoise);
SerialCommand serialCommandI("I", requestProgramId);

void setup()
{
    Serial.begin(9600);
    otto.ledMatrix.init(OTTO_LED_MATRIX_DIN_PIN, OTTO_LED_MATRIX_CLK_PIN, OTTO_LED_MATRIX_CS_PIN);
    otto.ledMatrix.putBitmap(OttoBitmap::plug);
    otto.buzzer.init(OTTO_BUZZER_PIN);
    otto.buzzer.playSong(OttoSong::connection);
    otto.init(OTTO_LEFT_LEG_PIN, OTTO_RIGHT_LEG_PIN, OTTO_LEFT_FOOT_PIN, OTTO_RIGHT_FOOT_PIN);
    ultrasonic.init(OTTO_US_TRIGGER_PIN, OTTO_US_ECHO_PIN);
    noise.init(OTTO_NOISE_PIN);
    otto.home();
    otto.ledMatrix.putBitmap(OttoBitmap::smile);
    otto.buzzer.playSong(OttoSong::happy);

    serialCommands.AddCommand(&serialCommandS);
    serialCommands.AddCommand(&serialCommandL);
    serialCommands.AddCommand(&serialCommandT);
    serialCommands.AddCommand(&serialCommandM);
    serialCommands.AddCommand(&serialCommandH);
    serialCommands.AddCommand(&serialCommandK);
    serialCommands.AddCommand(&serialCommandM);
    serialCommands.AddCommand(&serialCommandH);
    serialCommands.AddCommand(&serialCommandK);
    serialCommands.AddCommand(&serialCommandG);
    serialCommands.AddCommand(&serialCommandR);
    serialCommands.AddCommand(&serialCommandE);
    serialCommands.AddCommand(&serialCommandD);
    serialCommands.AddCommand(&serialCommandN);
    serialCommands.AddCommand(&serialCommandI);
    serialCommands.SetDefaultHandler(defaultHandler);
}

void loop()
{
    if (Serial.available())
    {
        serialCommands.ReadSerial();
    }

    if (moveId > -1)
    {
        move();
    }
}
